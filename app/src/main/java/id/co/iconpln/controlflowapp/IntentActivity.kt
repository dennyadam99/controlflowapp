package id.co.iconpln.controlflowapp

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_intent.*


class IntentActivity : AppCompatActivity(), View.OnClickListener {

    private val REQUEST_CODE = 110

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intent)
        setOnClickListener()


    }

    private fun setOnClickListener() {

        btnIntentMoveActivity.setOnClickListener(this)
        btnIntentMoveActivityWithData.setOnClickListener(this)
        btnIntentMoveActivityWithBundle.setOnClickListener(this)
        btnIntentMoveActivityWithObject.setOnClickListener(this)
        btnIntentImplicit.setOnClickListener(this)
        btnIntentWithResult.setOnClickListener(this)
        btnOpenWeb.setOnClickListener(this)
        btnSendSms.setOnClickListener(this)
        btnShowMap.setOnClickListener(this)
        btnShareText.setOnClickListener(this)
    }

    override fun onClick(view: View) {

        when (view.id) {
            R.id.btnIntentMoveActivityWithData -> {
                val moveIntentWithDataIntent =
                    Intent(this, MoveActivityWithDataActivity::class.java)
                moveIntentWithDataIntent.putExtra(MoveActivityWithDataActivity.EXTRA_NAME, "Denny")
                moveIntentWithDataIntent.putExtra(MoveActivityWithDataActivity.EXTRA_AGE, 21)
                startActivity(moveIntentWithDataIntent)
            }

            R.id.btnIntentMoveActivityWithBundle -> {
                val moveIntentWithBundleIntent =
                    Intent(this, MoveActivityWithBundleActivity::class.java)
                val bundle = Bundle()
                bundle.putString(MoveActivityWithBundleActivity.EXTRA_BUNDLE_NAME, "Denny Adam")
                bundle.putInt(MoveActivityWithBundleActivity.EXTRA_BUNDLE_AGE, 21)
                moveIntentWithBundleIntent.putExtras(bundle)
                startActivity(moveIntentWithBundleIntent)
            }
            R.id.btnIntentMoveActivityWithObject -> {
                val moveIntentWithObjectIntent =
                    Intent(this, MoveActivityWithObjectActivity::class.java)
                val person = Person("Denny", 17, "denny@mail.com", "jogja")
                moveIntentWithObjectIntent.putExtra(
                    MoveActivityWithObjectActivity.EXTRA_PERSON,
                    person
                )
                startActivity(moveIntentWithObjectIntent)

            }
            R.id.btnIntentWithResult -> {
                val resultIntent = Intent(this, ResultActivity::class.java)
                startActivityForResult(resultIntent, REQUEST_CODE)
            }
            R.id.btnIntentImplicit -> {
                val implicit = "0882111"
                val ImplicitIntent = Intent(Intent.ACTION_DIAL, Uri.parse("tel: $implicit"))
                startActivity(ImplicitIntent)
            }
            R.id.btnOpenWeb -> {
                val webpage = Uri.parse("https://www.binar.co.id")
                val openWebIntent = Intent(Intent.ACTION_VIEW, webpage)
                if (openWebIntent.resolveActivity(packageManager) != null) {
                    startActivity(openWebIntent)
                }
            }
            R.id.btnSendSms -> {
                val phoneNumber = "08826171212"
                val sendSms = Uri.parse("smsto: $phoneNumber")
                val message = "Hello, Saya denny"
                val sendSmsIntent = Intent(Intent.ACTION_SENDTO, sendSms)
                sendSmsIntent.putExtra(
                    "sms_body", message
                )
                if (sendSmsIntent.resolveActivity(packageManager) != null) {
                    startActivity(sendSmsIntent)
                }
            }
            R.id.btnShowMap ->{

                val latitude = "47.6"
                val longitude = "-122.3"
                val showMap = Uri.parse("geo: $latitude, $longitude")
                val showMapIntent = Intent(Intent.ACTION_VIEW, showMap)
                if(showMapIntent.resolveActivity(packageManager) != null){
                    startActivity(showMapIntent)
                }
            }
            R.id.btnShareText ->{
                val sharedText ="SHARE DONG"
                val shareTextIntent = Intent(Intent.ACTION_SEND)
                shareTextIntent.putExtra(Intent.EXTRA_TEXT, sharedText)
                shareTextIntent.type= "TEXT/PLAIN"
                val shareIntent = Intent.createChooser(shareTextIntent, "kemana nih")
                if(shareTextIntent.resolveActivity(packageManager) != null) {
                    startActivity(shareIntent)
                }
            }
        }
    }
        override fun onActivityResult(
            requestCode: Int, resultCode: Int, data: Intent?
        ) {
            super.onActivityResult(requestCode, resultCode, data)

            if (requestCode == REQUEST_CODE) {
                if (resultCode == ResultActivity.RESULT_CODE) {
                    val selectedValue = data?.getIntExtra(ResultActivity.EXTRA_VALUE, 0)
                    tvResultIntent.text = "Hasilnya $selectedValue"
                }
            }
        }

    }
