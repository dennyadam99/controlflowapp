package id.co.iconpln.controlflowapp.model.myContact

data class PhoneResponse(
    val home: String,
    val mobile: String,
    val office: String
)