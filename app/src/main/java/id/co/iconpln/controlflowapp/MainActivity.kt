package id.co.iconpln.controlflowapp

import android.net.sip.SipSession
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        etNilai.setText("0")
        btnShow.setOnClickListener {
            if (etNilai.text.isNotEmpty()) {
                val angka = etNilai.text.toString().toInt()
                hitungpangkat(angka)
            }
        }
    }

    fun hitungpangkat(angka: Int) {

        val hitungpangkat = angka * angka
        tvHasil.text = "Hasilnya: $hitungpangkat"

    }
}




