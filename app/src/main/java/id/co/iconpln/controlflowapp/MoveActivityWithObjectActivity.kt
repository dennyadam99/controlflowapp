package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.co.iconpln.controlflowapp.model.Person
import kotlinx.android.synthetic.main.activity_move_with_object.*

class MoveActivityWithObjectActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_PERSON = "extra_person"
    }

    private lateinit var person:Person

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_with_object)

        getIntentExtars()
        showData()
    }

    private fun getIntentExtars() {
        person = intent.getParcelableExtra(EXTRA_PERSON)
    }

    private fun showData() {

        val text =
            "Nama: ${person.name}, \nUmur: ${person.age} \nEmail: ${person.email} \nCity: ${person.city}"
        tvDataReceiveObject.text = text
    }
}
