package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        btnShowLogin.setOnClickListener {
            checkfield()

        }
    }

    fun checkfield(){

        tvStatuslogin.text ="Status Login :"
        when{
            etUsername.text.isNullOrEmpty() -> etUsername.error ="Username tidak boleh kosong"
            etPassword.text.isNullOrEmpty() -> etPassword.error = "Password tidak boleh kosong"
            etPassword.text.length < 7 -> etPassword.error="Password Tidak Boleh Kurang dari 7 Karakter"
            !Patterns.EMAIL_ADDRESS.matcher(
                etUsername.text).matches() -> etUsername.error = "Format email salah"
        else ->{
            doLogin(etUsername.text.toString(), etPassword.text.toString())

       doLogin(etUsername.text.toString(), etPassword.text.toString())

        }
    }
}

    private fun doLogin(username: String, password: String) {
        var loginStatus = ""
        if (username.equals("user@mail.com", false)
            && password.equals("password", false)) {
            loginStatus = "Status Login: Berhasil"
        } else {
            loginStatus = "Status Login: Gagal"
        }
        tvStatuslogin.text = loginStatus
    }
}
