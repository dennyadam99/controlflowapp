package id.co.iconpln.controlflowapp.myUserFavorite

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser

import kotlinx.android.synthetic.main.item_list_my_user.view.*

class MyUserFavoriteAdapter : RecyclerView.Adapter<MyUserFavoriteAdapter.MyUserFavoriteViewHolder>() {

    private lateinit var onItemClickCallback: OnItemClickCallback

    private var myUserFavoriteData = emptyList<FavoriteUser>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyUserFavoriteViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.item_list_my_user, parent, false)
        return MyUserFavoriteViewHolder(view)
    }

    override fun getItemCount(): Int {
        return myUserFavoriteData.size
    }

    override fun onBindViewHolder(holder: MyUserFavoriteViewHolder, position: Int) {
        holder.bind(myUserFavoriteData[position])
        holder.itemView.setOnClickListener{
            onItemClickCallback.onItemClick(myUserFavoriteData[holder.adapterPosition])
        }
    }
    fun setOnItemClickCallback(onItemClickCallback: OnItemClickCallback){
        this.onItemClickCallback = onItemClickCallback
    }
    fun setData(userItems: List<FavoriteUser>) {

        val listFavUser = ArrayList<FavoriteUser>()
        for (i in 0 until userItems.size){
            listFavUser.add(userItems[i])
        }
        myUserFavoriteData = listFavUser
        notifyDataSetChanged()
    }


    inner class MyUserFavoriteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(myUserItem: FavoriteUser) {
            itemView.tvUserName.text = myUserItem.userName
            itemView.tvUserAddress.text = myUserItem.userAddress
            itemView.tvUserMobile.text = myUserItem.userPhone
        }

    }

    interface OnItemClickCallback{
        fun onItemClick (myUser: FavoriteUser)
    }
}