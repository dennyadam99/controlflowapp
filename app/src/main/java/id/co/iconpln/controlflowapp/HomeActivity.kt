package id.co.iconpln.controlflowapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import id.co.iconpln.controlflowapp.bottomSheetDialog.BottomSheetActivity
import id.co.iconpln.controlflowapp.contact.ContactActivity
import id.co.iconpln.controlflowapp.contactFragment.ContactTabActivity
import id.co.iconpln.controlflowapp.fragmentBottomNav.BottomNavActivity
import id.co.iconpln.controlflowapp.fragmentNavDrawer.NavDrawerActivity
import id.co.iconpln.controlflowapp.fragmentTab.TabActivity
import id.co.iconpln.controlflowapp.fragmentViewPager.ScrollActivity
import id.co.iconpln.controlflowapp.fragments.DemoFragmentActivity
import id.co.iconpln.controlflowapp.hero.ListHeroActivity
import id.co.iconpln.controlflowapp.myContact.MyContactActivity
import id.co.iconpln.controlflowapp.myProfile.MyProfileActivity
import id.co.iconpln.controlflowapp.myProfileLogin.MyProfileLoginActivity
import id.co.iconpln.controlflowapp.myUser.MyUserActivity
import id.co.iconpln.controlflowapp.sharedPreferences.SharedPreferencesActivity
import id.co.iconpln.controlflowapp.weather.WeatherActivity
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity(), View.OnClickListener {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setOnClickListener()


    }

    private fun setOnClickListener() {

        btnCalculator.setOnClickListener(this)
        btnKlasifikasi.setOnClickListener(this)
        btnLogin.setOnClickListener(this)
        btnOperation.setOnClickListener(this)
        btnStyle.setOnClickListener(this)
        btnActivity.setOnClickListener(this)
        btnVolume.setOnClickListener(this)
        btnIntent.setOnClickListener(this)
        btnComplexConstraint.setOnClickListener(this)
        btnConstraint.setOnClickListener(this)
        btnListHero.setOnClickListener(this)
        btnGridHero.setOnClickListener(this)
        btnDemoFragment.setOnClickListener(this)
        btnTabHome.setOnClickListener(this)
        btnBottomNav.setOnClickListener(this)
        btnNavDrawer.setOnClickListener(this)
        btnBottomSheet.setOnClickListener(this)
        btnLocalization.setOnClickListener(this)
        btnScroll.setOnClickListener(this)
        btnPreferences.setOnClickListener(this)
        btnWeather.setOnClickListener(this)
        btnContact.setOnClickListener(this)
        btnBackgroundThread.setOnClickListener(this)
        btnContactTab.setOnClickListener(this)
        btnMyContact.setOnClickListener(this)
        btnMyUser.setOnClickListener(this)
        btnMyProfile.setOnClickListener(this)

    }

    override fun onClick(view: View) {

        when (view.id) {
            R.id.btnCalculator -> {
                val calculatorIntent = Intent(this, MainActivity::class.java)
                startActivity(calculatorIntent)

            }

            R.id.btnKlasifikasi -> {
                val klasifikasiIntent = Intent(this, ClassificationActivity::class.java)
                startActivity(klasifikasiIntent)

            }

            R.id.btnLogin -> {
                val loginIntent = Intent(this, LoginActivity::class.java)
                startActivity(loginIntent)
            }
            R.id.btnOperation -> {
                val operationIntent = Intent(this, OperationActivity::class.java)
                startActivity(operationIntent)
            }
            R.id.btnStyle -> {
                val styleIntent = Intent(this, StyleActivity::class.java)
                startActivity(styleIntent)
            }
            R.id.btnActivity -> {
                val activityIntent = Intent(this, DemoActivity::class.java)
                startActivity(activityIntent)
            }
            R.id.btnVolume -> {
                val volumeIntent = Intent(this, VolumeActivity::class.java)
                startActivity(volumeIntent)
            }
            R.id.btnIntent -> {
                val intentIntent = Intent(this, IntentActivity::class.java)
                startActivity(intentIntent)
            }
            R.id.btnComplexConstraint -> {
                val complexIntent = Intent(this, ComplexConstraintActivity::class.java)
                startActivity(complexIntent)
            }
            R.id.btnConstraint -> {
                val constraintIntent = Intent(this, ConstraintActivity::class.java)
                startActivity(constraintIntent)
            }
            R.id.btnListHero -> {
                val listHeroIntent = Intent(this, ListHeroActivity::class.java)
                startActivity(listHeroIntent)
            }

            R.id.btnGridHero -> {
                val gridHeroIntent = Intent(this, GridHeroActivity::class.java)
                startActivity(gridHeroIntent)
            }
            R.id.btnDemoFragment -> {
                val demoFragmentIntent = Intent(this, DemoFragmentActivity::class.java)
                startActivity(demoFragmentIntent)
            }
            R.id.btnTabHome -> {
                val tabIntent = Intent(this, TabActivity::class.java)
                startActivity(tabIntent)
            }
            R.id.btnBottomNav -> {
                val bottomIntent = Intent(this, BottomNavActivity::class.java)
                startActivity(bottomIntent)
            }
            R.id.btnNavDrawer -> {
                val drawerIntent = Intent(this, NavDrawerActivity::class.java)
                startActivity(drawerIntent)
            }
            R.id.btnBottomSheet -> {
                val bottomSheetIntent = Intent(this, BottomSheetActivity::class.java)
                startActivity(bottomSheetIntent)
            }
            R.id.btnLocalization -> {
                val localizationIntent = Intent(this, LocalizationActivity::class.java)
                startActivity(localizationIntent)
            }
            R.id.btnScroll -> {
                val scrollIntent = Intent(this, ScrollActivity::class.java)
                startActivity(scrollIntent)
            }
            R.id.btnPreferences -> {
                val preferencesIntent = Intent(this, SharedPreferencesActivity::class.java)
                startActivity(preferencesIntent)
            }
            R.id.btnWeather -> {
                val weatherIntent = Intent(this, WeatherActivity::class.java)
                startActivity(weatherIntent)
            }
            R.id.btnContact -> {
                val contactIntent = Intent(this, ContactActivity::class.java)
                startActivity(contactIntent)
            }
            R.id.btnBackgroundThread -> {
                val threadIntent = Intent(this, BackgroundThreadActivity::class.java)
                startActivity(threadIntent)
            }
            R.id.btnContactTab -> {
                val contactTabIntent = Intent(this, ContactTabActivity::class.java)
                startActivity(contactTabIntent)
            }
            R.id.btnMyContact -> {
                val myContactIntent = Intent(this, MyContactActivity::class.java)
                startActivity(myContactIntent)
            }
            R.id.btnMyUser -> {
                val myUserIntent = Intent(this, MyUserActivity::class.java)
                startActivity(myUserIntent)
            }
            R.id.btnMyProfile -> {
                val myProfileIntent = Intent(this, MyProfileActivity::class.java)
                startActivity(myProfileIntent)
            }
        }
    }
}