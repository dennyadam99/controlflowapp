package id.co.iconpln.controlflowapp.fragments


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import id.co.iconpln.controlflowapp.R
import kotlinx.android.synthetic.main.fragment_last.*
import id.co.iconpln.controlflowapp.StyleActivity
import kotlinx.android.synthetic.main.fragment_home.*


/**
 * A simple [Fragment] subclass.
 */
class LastFragment : Fragment(), View.OnClickListener {

    companion object {
        const val EXTRA_NAME_FRAGMENT = "extra_name_fragment"
        var message: String = ""
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_last, container, false)
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_other_activity.setOnClickListener(this)
        btnShowDialog.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id){
            R.id.btn_other_activity ->{
                val styleIntent = Intent(requireContext(), StyleActivity::class.java)
                startActivity(styleIntent)

            }
            R.id.btnShowDialog ->{
                val fragmentManager = childFragmentManager
                val optionDialogFragment = OptionDialogFragment()
                optionDialogFragment.show(
                    fragmentManager, OptionDialogFragment::class.java.simpleName
                )
            }

        }
    }

    var optionDialogListener: OptionDialogFragment.OnOptionDialogListener= object : OptionDialogFragment.OnOptionDialogListener{
        override fun onOptionDialogChosen(text: String) {
            Toast.makeText(requireContext(), text, Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val name =arguments?. getString(EXTRA_NAME_FRAGMENT)
        tvFirstName.text = name
        tvLastName.text = message
    }

 }
