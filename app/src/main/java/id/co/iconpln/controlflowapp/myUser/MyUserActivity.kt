package id.co.iconpln.controlflowapp.myUser

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import id.co.iconpln.controlflowapp.myUserFavorite.MyUserFavoriteActivity
import id.co.iconpln.controlflowapp.myUserForm.MyUserFormActivity
import kotlinx.android.synthetic.main.activity_my_user.*

class MyUserActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var adapter: MyUserAdapter
    private lateinit var myUserViewModel: MyUserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user)

        showMyUserList()
        initViewModel()
        fetchContactData()
        setOnClickListener()

        fetchUserData()

    }

    private fun fetchUserData() {

    }

    override fun onResume() {
        super.onResume()
        fetchUserData()
    }

    private fun setOnClickListener() {
        fabMyUserAdd.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.fabMyUserAdd -> {
                val userFormEditIntent = Intent(this, MyUserFormActivity::class.java)
                startActivity(userFormEditIntent)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_myuser, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_show_favorite -> {
                val myUserFavIntent = Intent(this, MyUserFavoriteActivity::class.java)
                startActivity(myUserFavIntent)
                true
            }
            else -> true
        }
    }

    private fun fetchContactData() {
        myUserViewModel.getListUser().observe(this, Observer { userItem ->
            if (userItem != null) {
                adapter.setData(userItem)
            }
        })
    }

    private fun showMyUserList() {
        adapter = MyUserAdapter()
        adapter.notifyDataSetChanged()

        rvMyUserList.layoutManager = LinearLayoutManager(this)
        rvMyUserList.adapter = adapter

        adapter.setOnItemClickCallback(object : MyUserAdapter.OnItemClickCallback {
            override fun onItemClick(myUser: UserDataResponse) {
                val userIntent = Intent(this@MyUserActivity, MyUserFormActivity::class.java)
                userIntent.putExtra(MyUserFormActivity.EXTRA_USER_EDIT, true)
                userIntent.putExtra(MyUserFormActivity.EXTRA_USER_ID, myUser.id)
                startActivity(userIntent)
            }
        })
    }

    private fun initViewModel() {
        myUserViewModel = ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        ).get(MyUserViewModel::class.java)
    }
}
