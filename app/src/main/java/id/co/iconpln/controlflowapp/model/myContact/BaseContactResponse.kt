package id.co.iconpln.controlflowapp.model.myContact

data class BaseContactResponse<T>(
    val contacts: List<ContactResponse>
)