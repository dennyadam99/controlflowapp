package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_operation.*

class OperationActivity : AppCompatActivity(), View.OnClickListener {

    private var inputX: Int = 0
    private var inputY: Int = 0

    lateinit var operationViewModel: OperationViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_operation)

        initViewModel()
        displayResult()
        setButtonClickListener()
        getInputNumbers()
    }

    private fun displayResult() {
        tvOperator.text = operationViewModel.operation
        tvOpResult.text = operationViewModel.operationResult.toString()
    }

    private fun initViewModel() {
        operationViewModel = ViewModelProviders.of(this).get(OperationViewModel::class.java)
    }

    private fun getInputNumbers() {
        if (etBilanganX.text?.isNotEmpty() == true ||
            etBilanganY.text?.isNotEmpty() == true
        ) {

            inputX = etBilanganX.text.toString().toInt()
            inputY = etBilanganY.text.toString().toInt()
        }
    }

    private fun checkError(): Boolean {
        if (etBilanganX.text.isNullOrBlank() || etBilanganY.text.isNullOrBlank() || etBilanganX.text.isNullOrEmpty() || etBilanganY.text.isNullOrEmpty()) {
            tvOpResult.text = "Not Define"
            return false
        } else {
            return true
        }
    }

    private fun setButtonClickListener() {
        btnOpAdd.setOnClickListener(this)
        btnOpDivide.setOnClickListener(this)
        btnOpMultiply.setOnClickListener(this)
        btnOpSubstract.setOnClickListener(this)
        btnReset.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnOpAdd -> {
                operationViewModel.operation = "+"
                getInputNumbers()
                val add = Operation.Add(inputX)
                operationViewModel.execute(inputY, add)
                displayResult()
            }
            R.id.btnOpDivide -> {
                var checkAngka = checkError()
                if (checkAngka && etBilanganY.text.toString().toInt() != 0) {

                    getInputNumbers()
                    operationViewModel.operation = resources.getString(R.string.divide_operator)
                    val divide = Operation.Divide(inputX)
                    operationViewModel.execute(inputY, divide)
                    displayResult()
                }
            }
            R.id.btnOpMultiply -> {
                getInputNumbers()
                operationViewModel.operation = resources.getString(R.string.multiply_operator)
                val multiply = Operation.Multiply(inputX)
                operationViewModel.execute(inputY, multiply)
                displayResult()
            }
            R.id.btnOpSubstract -> {
                getInputNumbers()
                operationViewModel.operation = resources.getString(R.string.substract_operator)
                val substract = Operation.Substract(inputX)
                operationViewModel.execute(inputY, substract)
                displayResult()
            }
            R.id.btnReset -> {
                operationViewModel.operation = ""
                etBilanganX.setText("0")
                etBilanganY.setText("0")
                operationViewModel.operationResult = 0
                tvOperator.text = ""
                displayResult()
            }

        }
    }

}