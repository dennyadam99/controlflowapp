package id.co.iconpln.controlflowapp.myUserForm

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import id.co.iconpln.controlflowapp.R
import id.co.iconpln.controlflowapp.database.FavoriteUser
import id.co.iconpln.controlflowapp.database.FavoriteViewModel
import id.co.iconpln.controlflowapp.model.myUser.UserDataResponse
import kotlinx.android.synthetic.main.activity_my_user_form.*

class MyUserFormActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val EXTRA_USER_EDIT = "extra_user_edit"
        const val EXTRA_USER_ID = "extra_user_id"
    }

    private lateinit var myUserFormViewModel: MyUserFormViewModel
    private lateinit var favoriteVieModel: FavoriteViewModel
    private var userId: Int? = null
    private var isEditUser = false
    private var menuItem: Menu? = null
    private var isFavorite: Boolean = false
    private var  favoriteUserId: Long? =null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_user_form)

        initViewModel()
        setOnClickListener()
        initIntentExtra()
        checkForm(isEditUser)
    }


    private fun fetchUserData() {
        pbMyUserFormLoading.visibility = View.VISIBLE
        llMyUserFormContent.visibility = View.GONE
        getUser(userId as Int)
    }

    private fun checkForm(editUser: Boolean) {
        if (editUser) {
            fetchUserData()
        } else {
            btnFormSave.visibility = View.GONE
            btnFormDelete.visibility = View.GONE
        }
    }

    private fun initViewModel() {
        myUserFormViewModel = ViewModelProvider(
            this, ViewModelProvider.NewInstanceFactory()
        ).get(MyUserFormViewModel::class.java)

        favoriteVieModel =
            ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory(application))
                .get(FavoriteViewModel::class.java)
    }

    private fun setOnClickListener() {
        btnFormSave.setOnClickListener(this)
        btnFormDelete.setOnClickListener(this)
        btnFormAdd.setOnClickListener(this)

    }

    private fun initIntentExtra() {
        userId = intent.getIntExtra(EXTRA_USER_ID, 0)
        isEditUser = intent.getBooleanExtra(EXTRA_USER_EDIT, false)
    }

    private fun populateFormData(user: UserDataResponse) {
        etUserFormName.setText(user.name)
        etUserFormAddress.setText(user.address)
        etUserFormHp.setText(user.phone)

        btnFormSave.visibility = View.VISIBLE
        btnFormDelete.visibility = View.VISIBLE
        btnFormAdd.visibility = View.GONE
        //userId = user.id
    }

    private fun getUser(id: Int) {
        myUserFormViewModel.getUser(id).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User Loaded Successfully", Toast.LENGTH_SHORT).show()
                populateFormData(response)
                pbMyUserFormLoading.visibility = View.GONE
                llMyUserFormContent.visibility = View.VISIBLE
                setFavorite()
            } else {
                Toast.makeText(this, "Failed to Load User", Toast.LENGTH_SHORT).show()
                pbMyUserFormLoading.visibility = View.GONE

            }
        })
    }

    private fun setFavorite() {
        if (userId != null) {
            favoriteVieModel.getUser(userId as Int).observe(this, Observer { favoriteUser ->
                Log.d("Denny", "getUser $favoriteUser")
                isFavorite = favoriteUser != null
                setFavoriteIcon()

                if (favoriteUser != null){
                    favoriteUserId = favoriteUser.favUserId
                }
            })
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {

        menuInflater.inflate(R.menu.menu_favorite, menu)
        menuItem = menu
        setFavoriteIcon()

        if (!isEditUser) {
            menu.findItem(R.id.action_favorite).isVisible = false
        }
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        return when (item.itemId) {
            R.id.action_favorite -> {

                if (llMyUserFormContent.visibility == View.GONE) {
                    Toast.makeText(this, "Can't Add to favorite", Toast.LENGTH_SHORT).show()
                    return false
                }

                isFavorite = !isFavorite
                setFavoriteIcon()
                addOrRemoveFavorite()
                true
            }
            else -> true
        }
    }

    private fun addOrRemoveFavorite() {
        if (isFavorite) {
            addToFavorite()
        } else {
            removeFromFavorite()
        }
        favoriteVieModel.getAllFavoriteUsers().observe(this, Observer { listFavUser ->
            if (listFavUser.isNotEmpty()) {
                for (i in 0 until listFavUser.size) {
                    Log.d("Denny", "" + listFavUser[i].favUserId + listFavUser[i].userName)
                }
            }
        })
    }

    private fun addToFavorite() {
        favoriteVieModel.insertUser(
            FavoriteUser(
                0, etUserFormAddress.text.toString(),
                userId.toString(), etUserFormName.text.toString(),
                etUserFormHp.text.toString()
            )
        )
    }

    private fun removeFromFavorite() {
        if (userId != null) {
            favoriteVieModel.deleteUser(userId as Int)
        }
        favoriteUserId = null
    }


    private fun setFavoriteIcon() {
        if (isFavorite)
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_favorite)
        else {
            menuItem?.getItem(0)?.icon = ContextCompat.getDrawable(this, R.drawable.ic_unfavorite)
        }
    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnFormSave -> {
                if (userId != null) {
                    val updateUserData = UserDataResponse(
                        etUserFormAddress.text.toString(),
                        userId ?: 0,
                        etUserFormName.text.toString(),
                        etUserFormHp.text.toString()
                    )
                    updateUser(userId as Int, updateUserData)

                }
            }
            R.id.btnFormDelete -> {
                deleteUser(userId as Int)
            }
            R.id.btnFormAdd -> {
                val createUserData = UserDataResponse(
                    etUserFormAddress.text.toString(), userId ?: 0,
                    etUserFormName.text.toString(),
                    etUserFormHp.text.toString()
                )
                createUser(createUserData)
            }
        }
    }

    private fun updateUser(id: Int, userData: UserDataResponse) {
        myUserFormViewModel.updateUser(id, userData).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User Updated Successfully", Toast.LENGTH_SHORT).show()
                updateFavoriteUser(userData)
                finish()
            } else {
                Toast.makeText(this, "Failed to update user", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun updateFavoriteUser(userDataResponse: UserDataResponse) {
        if (favoriteUserId != null){
            favoriteVieModel.updateUser(
                FavoriteUser(favoriteUserId as Long,
                    userDataResponse.address,
                    userDataResponse.id.toString(),
                    userDataResponse.name,
                    userDataResponse.phone

                )
            )
        }
    }

    private fun deleteUser(id: Int) {
        myUserFormViewModel.deleteUser(id).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User Deleted Successfully", Toast.LENGTH_SHORT).show()

                if (isFavorite) removeFromFavorite()
                finish()
            } else {
                Toast.makeText(this, "Failed to Deleted User", Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun createUser(userData: UserDataResponse) {
        myUserFormViewModel.createUser(userData).observe(this, Observer { response ->
            if (response != null) {
                Toast.makeText(this, "User Created Successfully", Toast.LENGTH_SHORT).show()
                finish()
            } else {
                Toast.makeText(this, "Failed to Create User", Toast.LENGTH_SHORT).show()
            }
        }
        )
    }
}
