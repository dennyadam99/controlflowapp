package id.co.iconpln.controlflowapp.database

import androidx.lifecycle.LiveData

class FavoriteDatabaseRepository (private val favDatabseDao : FavoriteDatabaseDao){
    val allFavUsers: LiveData<List<FavoriteUser>> = favDatabseDao.getAllUsers()

    fun insertUser(user: FavoriteUser){
        favDatabseDao.insertUser(user)
    }
    fun deleteUser(id: Int){
        favDatabseDao.deleteUser(id)
    }

    fun getUser(id: Int): LiveData<FavoriteUser>{
        return favDatabseDao.getFavUser(id)

    }

    fun updateUser(user: FavoriteUser) {
        return favDatabseDao.updateUser(user)
    }
}