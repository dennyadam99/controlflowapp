package id.co.iconpln.controlflowapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProviders
import kotlinx.android.synthetic.main.activity_volume.*

class VolumeActivity : AppCompatActivity() {

    private var volumeResult: Int = 0

    lateinit var volumeViewModel: VolumeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_volume)


        initViewModel()
        displayResult()
        setClickListener()
    }

    private fun initViewModel() {
        volumeViewModel = ViewModelProviders.of(this).get(VolumeViewModel::class.java)
    }

    private fun setClickListener() {
        btnHitung.setOnClickListener {
            val length = etLength.text.toString()
            val width = etWidth.text.toString()
            val height = etHeight.text.toString()

            when {
                length.isEmpty() -> etLength.error = "Empty Field"
                width.isEmpty() -> etWidth.error = "Empty Field"
                height.isEmpty() -> etHeight.error = "Empty Field"
                else -> {
                    volumeViewModel.calculate(length, width, height)
                    displayResult()
                }
            }
        }
    }

    private fun displayResult() {
        tvResult.text = volumeViewModel.volumeResult.toString()
    }

    fun calculate(length: String, width: String, height: String) {
        volumeResult = Integer.parseInt(length) * Integer.parseInt(width) * Integer.parseInt(height)
    }
}
